package main

import (
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

type game struct {
	isStarted bool
	port      string
	rows      []row
}

type row struct {
	columns []int
}

type boat struct {
	rows    []int
	columns []int
	shoots  []bool
}

var party game
var boats []boat
var boatsInLife int

func main() {
	rand.Seed(time.Now().UnixNano())

	// Création de la game
	party = game{
		isStarted: false,
		port:      strconv.Itoa(9000 + rand.Intn(100)),
	}

	go startGame()

	var otherUrl string

	fmt.Println("Votre flotte : http://localhost:" + party.port)
	fmt.Print("Quelle est l'URL de la flotte adverse ? ")
	fmt.Scan(&otherUrl)

	var x string
	var y string

	fmt.Println("\nLancement de la partie...")

	boatsInLife = 2

	for boatsInLife != 0 {
		fmt.Println("\nQuel coup souhaitez-vous jouer ? ")
		fmt.Print("x : ")
		fmt.Scan(&x)
		fmt.Print("y : ")
		fmt.Scan(&y)

		data := url.Values{}
		data.Set("x", x)
		data.Set("y", y)

		client := &http.Client{}
		r, _ := http.NewRequest(http.MethodPost, otherUrl+"/hit", strings.NewReader(data.Encode())) // URL-encoded payload
		r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		r.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

		resp, err := client.Do(r)

		if err != nil {
			log.Fatalln(err)
		}

		defer resp.Body.Close()

		b, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}

		fmt.Print(string(b))

		data2 := url.Values{}
		client2 := &http.Client{}
		r2, _ := http.NewRequest(http.MethodPost, otherUrl+"/boats", strings.NewReader(data2.Encode())) // URL-encoded payload
		r2.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		r2.Header.Add("Content-Length", strconv.Itoa(len(data2.Encode())))

		resp2, err := client2.Do(r2)

		if err != nil {
			log.Fatalln(err)
		}

		defer resp2.Body.Close()

		b2, err := io.ReadAll(resp2.Body)
		if err != nil {
			log.Fatalln(err)
		}

		boatsInLife, _ = strconv.Atoi(string(b2))
		fmt.Println(boatsInLife, "bateau(x) restant(s)")
	}

	fmt.Println("Aucun bateau ennemi restant, victoire !")
	fmt.Println("\nFin de partie.")
}

func startGame() {
	// Génération du tableau 10x10
	for i := 0; i < 10; i++ {
		tmpRow := row{}
		for j := 0; j < 10; j++ {
			tmpRow.columns = append(tmpRow.columns, 0)
		}
		party.rows = append(party.rows, tmpRow)
	}

	for i := 0; i < 2; i++ {
		length := rand.Intn(3)
		length = length + 2

		axeX := []int{}
		axeY := []int{}
		shoots := []bool{}

		randX := rand.Intn(9)
		randY := rand.Intn(9)

		for i := 0; i < length; i++ {
			if randX+i < 10 && randY+i < 10 {
				axeX = append(axeX, randX+i)
				axeY = append(axeY, randY)
				party.rows[randY].columns[randX+i] = 3

				shoots = append(shoots, false)
			}
		}

		boats = append(boats, boat{
			rows:    axeX,
			columns: axeY,
			shoots:  shoots,
		})
	}

	// Définition de début de partie
	party.isStarted = true

	http.HandleFunc("/board", boardHandler)
	http.HandleFunc("/boats", boatsHandler)
	http.HandleFunc("/hit", hitHandler)
	http.ListenAndServe(":"+party.port, nil)
}

func boardHandler(rw http.ResponseWriter, req *http.Request) {
	if party.isStarted == true {
		// Bateaux détruits
		var boatsDead []boat

		// Gestion des shoots
		for _, boat := range boats {
			shoots := len(boat.shoots)

			for index, shoot := range boat.shoots {
				if shoot { // Si il es touché
					party.rows[boat.columns[index]].columns[boat.rows[index]] = 2
					shoots--
				}
			}

			// Si le bateau n'a plus de vie
			if shoots == 0 {
				boatsDead = append(boatsDead, boat)
			}
		}

		fmt.Fprint(rw, "<html><body><table style='text-align:center;border-spacing:0;'>")

		fmt.Fprintln(rw, "<tr style='font-weight:bold;'><td></td><td style='border-bottom: solid;'>A</td><td style='border-bottom: solid;'>B</td><td style='border-bottom: solid;'>C</td><td style='border-bottom: solid;'>D</td><td style='border-bottom: solid;'>E</td><td style='border-bottom: solid;'>F</td><td style='border-bottom: solid;'>G</td><td style='border-bottom: solid;'>H</td><td style='border-bottom: solid;'>I</td><td style='border-bottom: solid;'>J</td></tr>")

		// Affichage des lignes
		for index, row := range party.rows {
			fmt.Fprint(rw, "<tr><td style='font-weight:bold;border-right:solid;'>", index+1, "</td>")

			for _, column := range row.columns {
				fmt.Fprint(rw, "<td>")
				if column == 0 {
					fmt.Fprint(rw, "🌊")
				} else if column == 1 {
					fmt.Fprint(rw, "❌")
				} else if column == 2 {
					fmt.Fprint(rw, "💥")
				} else if column == 3 {
					fmt.Fprint(rw, "🚢")
				}
				fmt.Fprint(rw, "</td>")
			}

			fmt.Fprint(rw, "</tr>")
		}
		fmt.Fprint(rw, "</table></body></html>")
	}
}

func boatsHandler(rw http.ResponseWriter, req *http.Request) {
	if party.isStarted == true {
		boatsAlive := len(boats) //5

		// Parcours des bateaux
		for _, boat := range boats {
			shoots := len(boat.shoots) //3

			for _, shoot := range boat.shoots {
				if shoot {
					shoots--
				}
			}

			if shoots == 0 {
				boatsAlive--
			}
		}

		// Affichage du nombre de bateaux en vie
		fmt.Fprint(rw, boatsAlive)
	}
}

func hitHandler(rw http.ResponseWriter, req *http.Request) {
	if party.isStarted == true {
		x, _ := strconv.Atoi(req.FormValue("x"))
		y, _ := strconv.Atoi(req.FormValue("y"))
		x--
		y--

		shooted := false

		for _, boat := range boats {
			for index := range boat.columns {
				if boat.columns[index] == y && boat.rows[index] == x {
					boat.shoots[index] = true
					shooted = true

					fmt.Fprintln(rw, "Touché !")
				}
			}
		}

		if !shooted {
			party.rows[y].columns[x] = 1
			fmt.Fprintln(rw, "Dans l'eau !")
		}
	}
}
